import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import './NewsCards.css';


class NewsCards extends React.Component {
  constructor(props){
    super(props);
    this.state={
      newsData: [],
      favorites: [],
      input: ''
    }
  }
  componentDidMount = () => {
    this.fetchApi();   
    this.setState({
      favorites: JSON.parse(localStorage.getItem('fav'))
    })
  }

  fetchApi = async() => {
    let response = await fetch('https://nl-static-site-assets.s3.ap-south-1.amazonaws.com/reports.json');
    let user = await response.json();
    this.setState({newsData: user.items})
  }

  addToFavorite = (res, e) => {
    e.preventDefault();
    const favId = e.target.id;
    // const {favorites} = this.state;
    let lists = this.state.favorites || []
    lists.push(favId);
    this.setState({
      favorites: lists
    })
    localStorage.setItem('fav', JSON.stringify(this.state.favorites))
  };

  removeFavorite = (res, e) => {
    e.preventDefault();
    const favId =  e.target.id;
    const {favorites} = this.state;

    const index = favorites.indexOf(favId);
    if (index > -1) {
      favorites.splice(index, 1);
    }

    this.setState({
      favorites: favorites
    })
    localStorage.setItem('fav', JSON.stringify(favorites));
  }

  searchFiter = (e) => {
    this.setState({
      input: e.target.value
    })
  }

  render(){
    const {newsData, favorites, input} = this.state;
    const searchData = newsData.filter(val => {
      return val.item.headline.toString().toLowerCase().indexOf(input.toString().toLowerCase()) !== -1;
    });
    return(
      <div>
        <div className="search" onSubmit={this.submit}>
          <form className="search-form" >
            <input type="text" placeholder="Search for news, authors, categories and more.." value={this.state.input} onChange={this.searchFiter} />
          </form>
        </div>
        { favorites && favorites.length > 0 ?
        <div className="fav">
          FAVOURITES : {favorites.length}
        </div> : null}
        <div className="box">
          {
            searchData.map ((newsRecord, i) =>
              <div className="card" key={i}>
                { favorites && favorites.includes(newsRecord.id) ?
                 <FontAwesomeIcon
                   icon={faHeart}
                   className="icon"
                   size="3x" />
                   : null
                }
                <img src="https://homepages.cae.wisc.edu/~ece533/images/airplane.png" alt={newsRecord.story['author-name']} width="100%" height="200px" />
                <h2>{newsRecord.item.headline}</h2>
                <a className="price" href={newsRecord.story.url}>Url link: {newsRecord.story.url}</a>
                <p>Author: {newsRecord.story['author-name']} </p>
                <p>

                { favorites && favorites.includes(newsRecord.id) ? 
                  <button 
                    id={newsRecord.id}
                    onClick={(e)=> this.removeFavorite(newsRecord, e)}>
                    Remove from Favourite
                  </button> : 
                  <button 
                    id={newsRecord.id}
                    onClick={(e)=> this.addToFavorite(newsRecord, e)}>
                    Add to Favourite
                  </button>
                }
                </p>
              </div>
            )
          }        
        </div>  
      </div>
    );
  }
}

export default NewsCards;

