#########API#########

##############
API response: https://nl-static-site-assets.s3.ap-south-1.amazonaws.com/reports.json

Notes:
##############

-The above is a dummy API response. The `items:` array will have the stories.
################

-Build a mobile responsive page using React.
###############

-The stories can be represented using a list of cards in a grid layout.
#######################
-You can mark any story as favorite which can be indicated by a heart icon, it should persist if I refresh the page. (Store it locally)

##########################
-You can search stories by "headline" (field in the API response

#####################################
Heroku Link
https://news-luandry.herokuapp.com/
